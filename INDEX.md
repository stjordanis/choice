# choice

Present a choice to the user and wait for a key (uses kitten instead of cats)


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## CHOICE.LSM

<table>
<tr><td>title</td><td>choice</td></tr>
<tr><td>version</td><td>4.4a</td></tr>
<tr><td>entered&nbsp;date</td><td>2003-07-15</td></tr>
<tr><td>description</td><td>Present a choice to the user and wait for a key (uses kitten instead of cats)</td></tr>
<tr><td>keywords</td><td>freedos</td></tr>
<tr><td>author</td><td>Jim Hall &lt;jhall@freedos.org&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Tom Ehlert &lt;te@drivesnapshot.de&gt;</td></tr>
<tr><td>platforms</td><td>DOS **requires KITTEN</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Choice</td></tr>
</table>
